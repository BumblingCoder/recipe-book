# Chicken Croissant Casserole
## Crunchy baked tofu with a sweet soy glaze over rice.
<!-- toc -->

Makes 4 servings

### Ingredients
- 2 rolls of canned croissant dough
- 2 cups of shredded chicken
- 2 cups of cheddar cheeese, or a mix of cheddar and mozzarella
- 1 can cream of chicken soup (or other cream of _____ soup)
- 1 cup of milk
- 1/4 tsp Salt
- 1/2 tsp Pepper
- 2 tsp Paprika

### Instructions
1. Pre-heat the oven to 350 degrees
1. In a large bowl mix the chicken, cheese, and spices.
1. Unroll the croissants, and separate them into squares. Press the seam in the middle of the square to prevent it splitting into two triangles.
1. Place chicken mixture in croissants, and fold corners over to make rough balls. These will have some holes in the sides, which is fine.
1. Mix the soup and milk together.
1. Pour a thin layer of the milk mixture in the bottom of a 9x13 pan.
1. Place the croissants in the pan.
1. Pour the milk mixture around the croissants.
1. Sprinkle any remaining chicken mixture over the top.
1. Bake uncovered for 30 minutes.
