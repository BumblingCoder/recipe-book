# Sweet Tofu Bowls
## Crunchy baked tofu with a sweet soy glaze over rice.
<!-- toc -->

Makes 2 servings

### Ingredients
- Tofu
  - 1 block of extra firm tofu
  - 1 tbsp corn starch
  - 1 tbsp soy sauce
  - 1 tbsp olive oil

- Glaze
  - 3 cloves of garlic, minced
  - 1 tbsp grated ginger
  - 1/4 cup soy sauce
  - 1/4 cup honey
  - 3 tbsp rice vinegar
  - 2 tbsp mirin
  - 1 tbsp sesame oil
  - 1/2 tsp crushed red pepper

- Rice Bowl
  - White rice
  - Green onions

### Instructions
1. Cut the tofu into bite-sized cubes. Place the cubes between sheets of paper towel or a towel, and then drain them by placing a baking sheet on the cubes and weighing it down with something heavy. I use a stack of plates. Leave the cubes draining for 45 minutes to an hour.
2. Cook enough white rice to make two rice bowls, about 1 cup dry. This should ideally finish at the same time as the tofu to make the bowl as fresh as possible.
3. Preheat the oven to 400 degrees Farenheit.
4. Combine the olive oil, soy sauce, and corn starch, and then toss with the tofu to eavenly coat.
5. Transfer tofu to a baking sheet and bake for 30 minutes, or until crispy and golden on the edges, flipping halfway through.
6. Combine the glaze ingredients in a medium sauce pan
7. Over medium-high heat combine boil the glaze until it is frothy and thick, about 10 minutes. The texture should be the same as when making soft carmel. Stir continuiously to prevent burning.
8. Transfer the tofu to a bowl and toss with the glaze and toss until it is evenly covered and the glaze begins to stiffen.
9. Place a serving of rice and half of the tofu in each bowl. Garnish with sliced green onions and serve.
